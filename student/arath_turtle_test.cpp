/*
   
 * 18-642 Unit Testing Example
 * Example code by Milda Zizyte
 * Edited by Alina Rath (Nov 1)
 * This file contains the tests needed to check every transition
 * and output for the states in the statechart as well as branch coverage
 * for compute functions as well as data coverage for different type of inputs.
 */

#include "arath_mock.h"
#include <CUnit/Basic.h>
#include <stdio.h>

void T1_and_S1() { //Testing T1 and Output of S1
  turtleMove obj  = actualStateChart(true,START_STATE);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move = obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_CHECK);
  CU_ASSERT_EQUAL(output_move, NONE);
}

void T1_and_S1_datacoverage() { //Testing T1 with bumped = false and Output of S1.
  
  turtleMove obj  = actualStateChart(false,START_STATE);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move = obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_CHECK);
  CU_ASSERT_EQUAL(output_move, NONE);
}

void T6_and_S4() { //Testing T6 (bumped = true) and Output of S4.
  turtleMove obj  = actualStateChart(true,CAN_MOVE);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move = obj.update;

  CU_ASSERT_EQUAL(state, ROTATE_CHECK);
  CU_ASSERT_EQUAL(output_move, MOVE_FORWARD);

}

void T6_and_S4_datacoverage() { //testing t6 (bumped = false) and Output of S4.
  turtleMove obj  = actualStateChart(false,CAN_MOVE);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_CHECK);
  CU_ASSERT_EQUAL(output_move, MOVE_FORWARD);
}


void T3_and_S2() { //testing T3 and Output of S2.
  set_numberofturns(3);
  turtleMove obj  = actualStateChart(true, ROTATE_CHECK);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_MOVE);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
}

void T2_and_S2() { //testing t2 and Output of S2.
  set_numberofturns(2);
  turtleMove obj  = actualStateChart(false, ROTATE_CHECK);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_CHECK);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
}

void T4_and_S3() { //testing T4 (local turns < min turns) and Output of S3.
  set_minturns(3);
  set_count_local_turns(1);
  turtleMove obj  = actualStateChart(false, ROTATE_MOVE);
  int32_t state = obj.current_state_turtle;
  
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_MOVE);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
}

void T5_and_S3() { //testing T5 (local turns == min_turns) + Output of S3.
  set_minturns(3);
  set_count_local_turns(2);
  turtleMove obj  = actualStateChart(false, ROTATE_MOVE);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, CAN_MOVE);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
}
//Data coverage + Branch coverage

void undefined_state() { //testing undefined state 
  turtleMove obj  = actualStateChart(false, 19); //undefined state
  int32_t state = obj.current_state_turtle;
  bool mock_error_local = get_mock_error();
  CU_ASSERT_EQUAL(mock_error_local, true);
}

void T2_and_S2_coverage() { //testing t2 and Output of S2.
  set_numberofturns(1);
  turtleMove obj  = actualStateChart(false, ROTATE_CHECK);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_CHECK);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
}

void T2_and_S2_coverage1() { //testing t2 and Output of S2.
  set_numberofturns(1);
  turtleMove obj  = actualStateChart(false, ROTATE_CHECK);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_CHECK);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
}

void T2_and_S2_coverage2() { //testing t2 and Output of S2.
  set_numberofturns(0);
  turtleMove obj  = actualStateChart(false, ROTATE_CHECK);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_CHECK);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
}

void T4_and_S3_coverage() { //testing T4 (local turns < min turns) and Output of S3.
  set_minturns(3);
  set_count_local_turns(0);
  turtleMove obj  = actualStateChart(false, ROTATE_MOVE);
  int32_t state = obj.current_state_turtle;
  
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_MOVE);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
}
void T5_and_S3_coverage() { //testing T5 (local turns ==  min turns) and Output of S3.
  set_minturns(4);
  set_count_local_turns(3);
  turtleMove obj  = actualStateChart(false, ROTATE_MOVE);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, CAN_MOVE);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
}
void T4_and_S3_coverage1() { //testing T4 (local turns < min turns) and Output of S3.
  set_minturns(4);
  set_count_local_turns(2);
  turtleMove obj  = actualStateChart(false, ROTATE_MOVE);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_MOVE);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
}
void test_computeFunc1() { //testing compute function find_min()
  int32_t elements[4] = {1,2,3,4};
  set_cell_visit_counts(elements);
  set_numberofturns(3);
  int32_t res = find_min();
  turtleMove obj  = actualStateChart(true, ROTATE_CHECK);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_MOVE);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
  CU_ASSERT_EQUAL(res, 0);
}
void test_computeFunc2() { //testing compute function find_min()
  int32_t elements[4] = {4,1,2,3};
  set_cell_visit_counts(elements);
  set_numberofturns(3);
  int32_t res = find_min();
  turtleMove obj  = actualStateChart(true, ROTATE_CHECK);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_MOVE);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
  CU_ASSERT_EQUAL(res, 1);
}

void test_computeFunc3() { //testing compute function find_min()
  int32_t elements[4] = {INT32_MAX,4,1,3};
  set_cell_visit_counts(elements);
  set_numberofturns(3);
  int32_t res = find_min();
  turtleMove obj  = actualStateChart(true, ROTATE_CHECK);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_MOVE);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
  CU_ASSERT_EQUAL(res, 2);
}

void test_computeFunc4() { //testing compute function find_min()
  int32_t elements[4] = {INT32_MAX,INT32_MAX,INT32_MAX,3};
  set_cell_visit_counts(elements);
  set_numberofturns(3);
  int32_t res = find_min();
  turtleMove obj  = actualStateChart(true, ROTATE_CHECK);
  int32_t state = obj.current_state_turtle;
  uint32_t output_move= obj.update;
  CU_ASSERT_EQUAL(state, ROTATE_MOVE);
  CU_ASSERT_EQUAL(output_move, TURN_RIGHT);
  CU_ASSERT_EQUAL(res, 3);
}


int init() {
  // Any test initialization code goes here
  return 0;
}

int cleanup() {
  // Any test cleanup code goes here
  return 0;
}

/* Skeleton code from http://cunit.sourceforge.net/example.html */
int main() {

  CU_pSuite pSuite = NULL;

  /* initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* add a suite to the registry */
  pSuite = CU_add_suite("Suite_1", init, cleanup);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* add the tests to the suite */
  if ((NULL == CU_add_test(pSuite, "test of transition T1", T1_and_S1)) ||
       (NULL == CU_add_test(pSuite, "Testing T1 data coverage", T1_and_S1_datacoverage)) ||
       (NULL == CU_add_test(pSuite, "test of transition T6", T6_and_S4)) ||
       (NULL == CU_add_test(pSuite, "Testing T6 data coverage", T6_and_S4_datacoverage)) ||
       (NULL == CU_add_test(pSuite, "test of transition T2", T2_and_S2)) ||
       (NULL == CU_add_test(pSuite, "test of transition T3", T3_and_S2)) ||
       (NULL == CU_add_test(pSuite, "test of transition T4", T4_and_S3)) ||
       (NULL == CU_add_test(pSuite, "test of transition T5", T5_and_S3)) ||
       (NULL == CU_add_test(pSuite, "Testing error state", undefined_state))   ||
       (NULL == CU_add_test(pSuite, "Testing T2 data coverage", T2_and_S2_coverage))  ||
       (NULL == CU_add_test(pSuite, "Testing T2 data coverage", T2_and_S2_coverage1))  ||
       (NULL == CU_add_test(pSuite, "Testing T2 data coverage", T2_and_S2_coverage2))  || 
       (NULL == CU_add_test(pSuite, "Testing T4 data coverage", T4_and_S3_coverage))  ||
       (NULL == CU_add_test(pSuite, "Testing T5 data coverage", T5_and_S3_coverage))  ||
       (NULL == CU_add_test(pSuite, "Testing T4 data coverage", T4_and_S3_coverage1))  ||
       (NULL == CU_add_test(pSuite, "Testing branch coverage- compute function", test_computeFunc1)) ||
       (NULL == CU_add_test(pSuite, "Testing branch coverage- compute function", test_computeFunc2)) || 
       (NULL == CU_add_test(pSuite, "Testing branch coverage- compute function", test_computeFunc3)) ||
       (NULL == CU_add_test(pSuite, "Testing branch coverage- compute function", test_computeFunc4)))
   {
      CU_cleanup_registry();
      return CU_get_error();
    }
  
  /* Run all tests using the CUnit Basic interface */
  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_basic_run_tests();
  CU_cleanup_registry();
  return CU_get_error();

  return 0;
}


