#include <ros/ros.h>
#include <boost/bind.hpp>
#include <ece642rtle/timeInt8.h>
#include <std_msgs/Empty.h>
#include <ece642rtle/RTIbump.h>
#include <ece642rtle/RTIatend.h>
#include <ece642rtle/PoseOrntBundle.h>
#include <ece642rtle/bumpEcho.h>
#include <ece642rtle/aendEcho.h>
#include <QPointF>

// Functions to interface with ROS. Don't change these lines!
bool bumped(int x1,int y1,int x2,int y2);
bool atend(int x, int y);
void displayVisits(int visits);
bool moveTurtle(QPointF& pos_, int& nw_or);
// Scope-preserving changes to these lines permitted (see p5 writeup)

//enum turtleMove {MOVE};
typedef struct{
    uint32_t update=0;
    int visit_count=0;
    int32_t current_state_turtle;
} turtleMove;

typedef enum{
   START_STATE = 0,
   ROTATE_CHECK = 1,
   ROTATE_MOVE = 2,
   CAN_MOVE = 3
} all_states_turtle;
QPointF translatePos(QPointF pos_, turtleMove nextMove, int &orie);
int& translateOrnt(int &orientation, turtleMove nextMove);
turtleMove studentTurtleStep(bool bumped);

// OK to change below this line
//enum to keep track of turtle's state
bool studentMoveTurtle(QPointF& pos_, int& nw_or);
typedef enum{
    CHECK_DIR = 0,
    VISITED_DIR = 1,
    RESET_DIR = 2
} current_state; 

//struct for geometric coupling of points
typedef struct{
    int32_t x = 0;
    int32_t y = 0;
} point;


//the orientation of the turtle wrt turtle nose
typedef enum{ 
    LEFT = 0,
    UP = 1,
    RIGHT = 2,
    DOWN = 3
} orientation;

//to interface between student turtle and maze
typedef enum{
    TURN_LEFT = 0,
    TURN_RIGHT = 1,
    MOVE_FORWARD = 2,
    NONE = 3
} update;

//int32_t min_turns = 0; 
//int32_t count_local_turns = 0;
//int32_t number_of_turns = 0;
//void set_numberofturns(int32_t num){
  //  number_of_turns=num;
//}

