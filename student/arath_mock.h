/*
 * 18-642 Unit Testing Example
 * Example code by Milda Zizyte
 * This is the header file for all unit tests
 * and mock functions.
 */

#include <stdint.h>
#include <iostream>

//THINGS TURTLEMOVE actually need
typedef enum{
    START_STATE,
    ROTATE_CHECK,
    ROTATE_MOVE, 
    CAN_MOVE
} move_state;
//enum orientation {UP, DOWN};
typedef enum{
    TURN_LEFT = 0,
    TURN_RIGHT = 1,
    MOVE_FORWARD = 2,
    NONE = 3
} update;



typedef struct{
    uint32_t update=0;
    int visit_count=0;
    int32_t current_state_turtle;
} turtleMove;

typedef enum{ 
    LEFT = 0,
    UP = 1,
    RIGHT = 2,
    DOWN = 3
} orientation;

int32_t find_min();
turtleMove actualStateChart(bool bumped, int32_t cs);

//GETTERS AND SETTERS
void set_numberofturns(int32_t num);
void set_minturns(int32_t num);
void set_count_local_turns(int32_t num);
bool get_mock_error();
void set_cell_visit_counts(int32_t elements[4]);
bool will_bump();

void ROS_ERROR(std::string e);
