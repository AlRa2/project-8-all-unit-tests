/*
 * 18-642 Unit Testing Example
 * Example code by Milda Zizyte
 */
#include <stdio.h>
#include "arath_mock.h"
#include <iostream>

//static orientation mock_orientation;
static bool mock_bump;
static bool mock_error = false;
static int32_t number_of_turns = 0;
/* Functions called by dummy_turtle */
/*void setOrientation(orientation ornt) {
  mock_orientation = ornt;
}*/

//void set_numberofturns(int32_t num){
  //  number_of_turns = num;
//}
bool will_bump() {
  return mock_bump;
}
bool get_mock_error(){
    return mock_error;
} 

/* Functions used to instrument CUnit tests */
/*orientation test_orientation_result() {
  return mock_orientation;
}*/

void mock_set_bump(bool bump) {
  mock_bump = bump;
}

/* Dummy ROS_ERROR */
void ROS_ERROR(std::string e) {
  mock_error = true;
  std::cout << e << std::endl;
}
