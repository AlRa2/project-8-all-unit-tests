/* 
 * Originally by Philip Koopman (koopman@cmu.edu)
 * and Milda Zizyte (milda@cmu.edu)
 *
 * STUDENT NAME: Alina Rath
 * ANDREW ID: arath
 * LAST UPDATE: Ocotober 11th
 *
 * This file keeps track of where the turtle is in the maze
 * and updates the location when the turtle is moved. It shall not
 * contain the maze solving logic/algorithm.
 *
 * This file is used along with student_turtle.cpp. student_turtle.cpp shall
 * contain the maze solving logic/algorithm and shall not make use of the
 * absolute coordinates or orientation of the turtle.
 *
 * This file shall call studentTurtleStep(..) in student_turtle.cpp to determine
 * the next move the turtle will make, and shall use translatePos(..) and
 * translateOrnt(..) to translate this move into absolute coordinates
 * to display the turtle.
 *
 */

#include "student.h"

/*
 * This procedure takes the current turtle position and orientation and returns true=accept changes, false=do not accept changes
 * Ground rule -- you are only allowed to call the three helper functions defined in student.h, and NO other turtle methods or maze methods (no peeking at the maze!)
 * This file interfaces with functions in student_turtle.cpp
 */


static uint32_t curr_state = 0;
const static uint32_t TIMEOUT =10;
/*
This funtion takes a position and a turtleMove and returns a new position
based on the move.
Inputs are Position of turtle in absolute co-ords, nextMove struct, and current orientation. 
Outputs are New position of the turtle after changes.
SF = 6.9
*/


QPointF translatePos(QPointF pos_, turtleMove nextMove, int &orie) {
   
    if(nextMove.update == MOVE_FORWARD){
        switch(orie){
            case UP:
              pos_.setY(pos_.y() - 1);
              break; 
            case DOWN: 
              pos_.setY(pos_.y() + 1);
              break;
            case LEFT:
              pos_.setX(pos_.x() - 1);
              break;
            case RIGHT:
              pos_.setX(pos_.x() + 1);
              break;
            default:
              ROS_INFO("Error message : should never reach here");
        }
    }
    return pos_;
}

         
/*
This function takes an orientation and a turtleMove and returns a new orienationbased on the move.
Inputs: new orientation of the turtle
Outputs: None as it modifies nw_or by reference.
SF = 5.9
 */
void change_direction(int &nw_or){
    switch(nw_or){
        case LEFT:
            nw_or = UP;
            break;
        case RIGHT:
            nw_or = DOWN;
            break;
        case UP:
            nw_or = RIGHT;
            break;
        case DOWN:
            nw_or = LEFT;
            break;
        default:
            ROS_INFO("Error, not a specified state");
    }
    curr_state = VISITED_DIR;
}

/* This function determines the nw_or just like the above 
but in the bumped case.
Inputs: new orientation of the turtle`
Outputs: None as it modifies nw_or by reference.
SF = 5.9
*/
void change_direction_bumped(int &nw_or){
    switch(nw_or){
        case LEFT:
            nw_or = DOWN;
            break;
        case RIGHT:
            nw_or = UP;
            break;
        case UP:
            nw_or = LEFT;
            break;
        case DOWN:
            nw_or = RIGHT;
            break;
        default:
            ROS_INFO("Error, not a specified state");
    }
    curr_state = CHECK_DIR;
}

/*This function translates what was recieved from turtle file
into an absolute orientation.
Inputs : Orientation and nextMove struct
Outputs: Orientation as an integer values.
SF = 3.7
*/



int& translateOrnt(int &orientation, turtleMove nextMove) {
    switch(nextMove.update){
       case TURN_RIGHT:
           change_direction(orientation);
           break;
       case TURN_LEFT:
           change_direction_bumped(orientation);
           break;  
       default: 
           curr_state = RESET_DIR;
    }
    return orientation;
}

/* This function checks the incoming position of the turtle and it's 
new position. 
Inputs: origin, destination and orientation.
Outputs: none, as it modifies orientation by reference.
SF = 4.95
*/

void check_incoming_pos(point &origin, point &destination, int &nw_or){
    if (nw_or < RIGHT){ 
        if (nw_or == LEFT){
            destination.y += 1;
        }
        else{
            destination.x += 1;
        }
    }
    else{
        destination.x += 1;
        destination.y += 1;
        if (nw_or == RIGHT){
            origin.x += 1;
        }
        else{
            origin.y += 1;
        }
    }
}


/*This is the main function of maze. It receives a position and an orientation.
It then checks to see if the turtle bumped and sends this info over to 
turtle file. It recieves back all the informatio it needs to mark the turtle's
co-ordinates (absolute form).
Inputs: Position (Absolute) and orientation
Outputs: boolean- Submit changes to turtle's position or not
SF = 6.35
*/
bool moveTurtle(QPointF& pos_, int &nw_or){
    static uint32_t countdown_timer = 0;
   
    bool at_end = atend((int)pos_.x(), (int)pos_.y());
      
    if (at_end == true){
        return false;
    }
    if(countdown_timer == 0){
     
        point origin;
        origin.x = (int)pos_.x();
        origin.y = (int)pos_.y();
        point destination = origin;
        check_incoming_pos(origin, destination, nw_or);

        bool bump = bumped(origin.x, origin.y, destination.x, destination.y); 

        turtleMove nextMove = studentTurtleStep(bump); 
        

	nw_or = translateOrnt(nw_or, nextMove); //turn the turtle


        if(nextMove.update == MOVE_FORWARD ){ //update cell visit counts
            displayVisits(nextMove.visit_count);
        }

        pos_ = translatePos(pos_, nextMove, nw_or); //turn the position


        countdown_timer = TIMEOUT;
        return true;
    }
    else{
        countdown_timer -= 1;
    }
    return false;
}     
