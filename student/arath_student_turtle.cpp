/* 
 * Originally by Philip Koopman (koopman@cmu.edu)
 * and Milda Zizyte (milda@cmu.edu)
 *
 * STUDENT NAME: Alina Rath
 * ANDREW ID: arath 
 * LAST UPDATE: October 31st
 *
 * This file is an algorithm to solve the ece642rtle maze
 * using the a greedy algorithm selecting the neighboring cell
 * with the lowest cell visit count.
*/ 

#ifdef testing
#include "arath_mock.h"
#endif
#ifndef testing
#include "student.h"
#include "ros/ros.h"
#endif

//#define static


//#include "arath_mock.h"
#include "stdio.h"
//#include "student.h"
// OK TO MODIFY BELOW THIS LINE
const static  int32_t ROWS = 23;//Size of the local map
const static  int32_t COLS = 23;
const static  int32_t NEIGHBORS = 4; //# of neighboring cells
const static  int32_t MIDPOINT = 11; //midpoint of 23x23 matrix is 11x11 

static int32_t localmap [ROWS][COLS] = {0}; //localmap to store turtle's local position
static int32_t cell_visit_counts[NEIGHBORS] = {0};
static int32_t min_turns = 0; 
static int32_t count_local_turns = 0;
static int32_t number_of_turns = 0;

static int32_t current_state_turtle = START_STATE;


/*This function is called when the turtle can move and has not bumped into a wall.
This function will make the turtle turn right from whatever it's current orientation is.
Inputs: The current state or movement, the orientation.
Outputs: Writes to the reference nw_or and sets curr_state to visited aka
we have checked right.
SF = 6.9
*/
void change_direction( uint32_t &nw_or){
    switch(nw_or){
        case LEFT:
            nw_or = UP;  
            break;
        case RIGHT:
            nw_or = DOWN;
            break;
        case UP:
            nw_or = RIGHT;
            break;
        case DOWN: 
            nw_or = LEFT;
            break;
        default:
            ROS_ERROR("Error, not a specified state");
    }
}

/*This function sets the next position of the turtle based on the
checks done on the corrent position.
Inputs: The orientation of the turtle in the maze and the struct to be returned
to maze.
Outputs: None
SF = 6.1 
*/
void set_next_position_local(uint32_t &nw_or, int32_t &local_pos_x, int32_t &local_pos_y){
   switch(nw_or){
        case UP: 
            local_pos_y = local_pos_y-1;
            break; 
        case RIGHT:
            local_pos_x = local_pos_x+1;
            break;
        case DOWN: 
            local_pos_y = local_pos_y+1;
            break;
        case LEFT:
            local_pos_x = local_pos_x-1;
            break;
        default:
            ROS_ERROR("this direction does not exist for the turtle");
    }
}


/*This function gets the value of the localmap at the position directly
ahead of the current position given the turtle's current orientation. 
Inputs:Value of bumped from maze function
Outputs: Returns the value of localmap at this position if not bumped, else
returns INT_MAX as we do not want to turn there.
SF: 6.7
*/
int32_t get_cell_visit_counts(bool bumped,uint32_t nw_or, int32_t local_pos_x, int32_t local_pos_y){ //localmap to store turtle's local position
    int32_t x = local_pos_x;
    int32_t y = local_pos_y;
    if(bumped){
        return INT32_MAX;
    }
    switch(nw_or){
        case UP: 
            y = y-1;
            break; 
        case RIGHT:
            x = x+1;
            break;
        case DOWN: 
            y = y+1;
            break;
        case LEFT:
            x = x-1;
            break;
        default:
            ROS_ERROR("this direction does not exist for the turtle");
    }
    return localmap[x][y];
}


/*This function finds the minimum of the cell visit counts of the 4 neighbors.
Inputs: None
Outputs: returns the position of the minimum of the neightbors visited.
SF: 2.45
*/
int32_t find_min(){
    int32_t min_pos = 0;
    int32_t min_visits = INT32_MAX;
    for(int32_t i = 0; i < NEIGHBORS;i++){
        if(cell_visit_counts[i] < min_visits){
            min_visits = cell_visit_counts[i];
            min_pos = i;
        }
    }
    return min_pos;
}

void set_numberofturns(int32_t num){
    number_of_turns=num;
}

void set_minturns(int32_t num){
    min_turns = num;
}

void set_count_local_turns(int32_t num){
    count_local_turns = num;
}

void set_cell_visit_counts(int32_t elements[NEIGHBORS]){
   for(int i = 0; i < NEIGHBORS; i++){
       cell_visit_counts[i] = elements[i];
    }
}

turtleMove actualStateChart(bool bumped, int32_t curr_state){
    static uint32_t nw_or = 0; //turtle's orientation
    static int32_t local_pos_x = 0; //recording the local position
    static int32_t local_pos_y = 0;
    turtleMove obj;

    switch(curr_state){
        case START_STATE: //S1
        {
            local_pos_x = MIDPOINT;
            local_pos_y = MIDPOINT;
            nw_or = UP;
            min_turns = 0;
            set_numberofturns(0);
            count_local_turns = 0;

            //OUTPUT of S1.
            obj.update = NONE;        //we are in the state state don't need to move right now
            //Transition logic
            obj.current_state_turtle = ROTATE_CHECK;
            current_state_turtle = ROTATE_CHECK; //T1
            break;
        }
        case ROTATE_CHECK: //S2
        { 
            int32_t a = get_cell_visit_counts(bumped, nw_or, local_pos_x, local_pos_y); //gets what is it front of us using C1.
            cell_visit_counts[number_of_turns] = a; //populate what is it in front of us.
           
            //actually rotate right locally
            change_direction(nw_or);
            //int32_t local_num = number_of_turns;
            //local_num +=1;
            //set_numberofturns(local_num);
            number_of_turns += 1;
            
            //OUTPUT of S2
            obj.update = TURN_RIGHT; //keep turning right while checking neighbors
            
            
            //Transition logic
            if(number_of_turns < 4){//stil gathering directions in ROTATE_CHECK state
                obj.current_state_turtle = ROTATE_CHECK;
                current_state_turtle = ROTATE_CHECK; //T2
            }
            else{     
            //done checking all 4 directions. Now we need to find the min. 
                min_turns = find_min(); //using C2
                if(min_turns == 0){
                    min_turns = NEIGHBORS;
                }
 
                count_local_turns = 0;
                //number_of_turns = 0;
                set_numberofturns(0);
                obj.current_state_turtle = ROTATE_MOVE;
                current_state_turtle = ROTATE_MOVE; //T3
            }
            break;
        }
   
        case ROTATE_MOVE: //S3
        {   
            change_direction(nw_or); //this updates nw_or locally, by turning it right
            count_local_turns += 1;
            //OUTPUT of S3
            obj.update = TURN_RIGHT;
 
           
            //Transtion logic
            if(count_local_turns < min_turns){ 

                obj.current_state_turtle = ROTATE_MOVE;
                current_state_turtle = ROTATE_MOVE; //T4
            }
            else{
                
                obj.current_state_turtle = CAN_MOVE;
                current_state_turtle = CAN_MOVE; //T5
            }
            break;
        }
        case CAN_MOVE: //S4
        {
            set_next_position_local(nw_or, local_pos_x, local_pos_y); //set the next position that changeslocal_X and localy
            localmap[local_pos_x][local_pos_y] += 1; //udate the map at that position
            obj.visit_count = localmap[local_pos_x][local_pos_y];
            //reset variables
            count_local_turns = 0;
            min_turns = 0;

            //OUTPUT of S4.
            obj.update = MOVE_FORWARD; //it can now move

            //Transition logic 
            current_state_turtle = ROTATE_CHECK; //T6
            obj.current_state_turtle = ROTATE_CHECK;
            break;
        }
        default:            
            ROS_ERROR("Undefined state");
       
    } 
    return obj; 
        
}

/*This is the main function that controls turtle step.
It has different states such as 
START_state: turtle step called for a new position
ROTATE_CHECK: in the process of checking the visit count for 4 neighbors.
ROTATE_MOVE: in the process of rotating to the least visited cell
CAN_MOVE: indicate to the turtle_maze that this is the new orientation and it should move forward.

Outputs: Returns an object to maze which contains information about its local orientation and state.
SF:10.05 
*/
turtleMove studentTurtleStep(bool bumped){
    int32_t curr_state = current_state_turtle;
    turtleMove obj = actualStateChart(bumped, curr_state);
    return obj; 
}
